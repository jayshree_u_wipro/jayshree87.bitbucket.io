def gitRepositoryUrl = "git@bitbucket.org:wiprodigitalcom/martech-demo"
def gitCredentialsId = "350ec6eb-08bc-4d52-b21a-34f23ef1d2b2"

def s3BucketName = 'martech.wiprodigital.com'
def exclusions = '--exclude "*_notes/*" --exclude "infrastructure/*" --exclude ".git/*" --exclude ".gitignore"'


stage("Update ${s3BucketName}") {
    node() {
        git poll: false, changelog: false, url: gitRepositoryUrl, credentialsId: gitCredentialsId
        sh "aws s3 sync . s3://${s3BucketName} --acl public-read ${exclusions} --delete"
    }
}
